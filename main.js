"use strict";

var THREECAMERA;


// callback : launched if a face is detected or lost. TODO : add a cool particle effect WoW !
function detect_callback(faceIndex, isDetected) {
  if (isDetected) {
    console.log('INFO in detect_callback() : DETECTED');
  } else {
    console.log('INFO in detect_callback() : LOST');
  }
}

// build the 3D. called once when Jeeliz Face Filter is OK
function init_threeScene(spec) {
  const threeStuffs = THREE.JeelizHelper.init(spec, detect_callback);

  let color = 0xFFFFFF;
  let intensity = 0.7;
  let lightA = new THREE.AmbientLight(color, intensity);
  threeStuffs.scene.add(lightA);

  color = 0xFFFFFF;
  intensity = 0.2;
  let light = new THREE.DirectionalLight(color, intensity);
  light.position.set(5, 10, 10);
  threeStuffs.scene.add(light);

  let helperMaterial = new THREE.MeshBasicMaterial({ color: 0xff00ff, wireframe: true });


  // CREATE A CUBE
  let threeCube = new THREE.Mesh(new THREE.BoxGeometry(1.2,1.7,1), helperMaterial);
  threeCube.position.set(0, .1, -.3);
  threeCube.frustumCulled = false;

  let eye_1 = new THREE.Mesh(new THREE.BoxGeometry(.3,.3,.3), new THREE.MeshNormalMaterial());
  eye_1.position.set(-.3, .2, .4);
  eye_1.frustumCulled = false;

  let eye_2 = new THREE.Mesh(new THREE.BoxGeometry(.3,.3,.3), new THREE.MeshNormalMaterial());
  eye_2.position.set(.3, .2, .4);
  eye_2.frustumCulled = false;

  let face = new THREE.Group();
  // face.add(threeCube);
  // face.add(eye_1);
  // face.add(eye_2);

  light.target = face;


  var objLoader = new THREE.OBJLoader2();
  objLoader.load('./obj/iron man head.obj', (event) => {

    const root = event.detail.loaderRootNode;
    root.position.set(0, .2, .2);
    root.scale.set( 1.1, 1.1, 1.1 );

    root.traverse(function(child) {
      console.log(child)
      if (child instanceof THREE.Mesh && child.name === "group1_polySurface7_group1_polySurface7_yellow")
        child.material = new THREE.MeshPhongMaterial( { color: new THREE.Color("#FDE267"), shininess: 100} );

      if (child instanceof THREE.Mesh && child.name === "group1_dpsBirailSurface9_group1_dpsBirailSurface9_Red")
        child.material = new THREE.MeshPhongMaterial( { color: new THREE.Color("#FD0505"), flatShading: true} );
    });

    face.add(root);
  });

  threeStuffs.faceObject.add(face);

  //CREATE THE CAMERA
  THREECAMERA = THREE.JeelizHelper.create_camera();
} // end init_threeScene()

// launched by body.onload():
function main(){
  JeelizResizer.size_canvas({
    canvasId: 'jeeFaceFilterCanvas',
    callback: function(isError, bestVideoSettings){
      init_faceFilter(bestVideoSettings);
    }
  })
} //end main()

function init_faceFilter(videoSettings){
  JEEFACEFILTERAPI.init({
    followZRot: true,
    canvasId: 'jeeFaceFilterCanvas',
    NNCpath: './js/jeelizFaceFilter/', // root of NNC.json file
    maxFacesDetected: 1,
    callbackReady: function(errCode, spec){
      if (errCode){
        console.log('AN ERROR HAPPENS. ERR =', errCode);
        return;
      }

      console.log('INFO : JEEFACEFILTERAPI IS READY');
      init_threeScene(spec);
    }, //end callbackReady()

    //called at each render iteration (drawing loop) :
    callbackTrack: function(detectState){
      THREE.JeelizHelper.render(detectState, THREECAMERA);
    } //end callbackTrack()
  }); //end JEEFACEFILTERAPI.init call
} // end main()

